import React, {Component, Fragment} from 'react';
import './App.css';
import {Switch, Route} from 'react-router-dom';
import Layout from './containers/Layout/Layout';
import ContactsPage from "./containers/ContactsPage/ContactsPage";
import HomePage from "./containers/HomePage/HomePage";
import AboutPage from "./containers/AboutPage/AboutPage";
import DivisionsPage from "./containers/DivisionsPage/DivisionsPage";
import ServicesPage from "./containers/ServicesPage/ServicesPage";
import AdminPage from "./containers/AdminPage/AdminPage";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Layout>
                    <Switch>
                        <Route path="/" exact component={HomePage} />
                        <Route path="/about" component={AboutPage} />
                        <Route path="/contacts" component={ContactsPage} />
                        <Route path="/divisions" component={DivisionsPage} />
                        <Route path="/services" component={ServicesPage} />
                        <Route path="/pages/admin" component={AdminPage} />
                        <Route render={() => <div>Not Found</div>} />
                    </Switch>
                </Layout>
            </Fragment>
        );
    }
}

export default App;
