import React from 'react';
import './Editor.css';

const Editor = props => {
    return (
        <div className="editor">
            <h3>Edit pages</h3>
            <select defaultValue="A" id="selector" onChange={props.page}>
                <option value="A" disabled>Select page</option>
                <option value="Home">Home</option>
                <option value="About">About</option>
                <option value="Contacts">Contacts</option>
                <option value="Divisions">Divisions</option>
                <option value="Services">Services</option>
            </select><br/>
            <label>
                <h4>Title</h4><br/>
                <input type="text" onChange={props.title} value={props.titleValue}/>
            </label><br/>
            <label>
                <h4>Content</h4><br/>
                <textarea cols="97" rows="8" onChange={props.content} value={props.contentValue}/>
            </label>
            <br/>
            <button onClick={props.send}>Save</button>
        </div>
    )
};

export default Editor;