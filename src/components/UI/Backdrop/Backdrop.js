import React, {Fragment} from 'react';
import './Backdrop.css';
import Preloader from '../../../assets/Preloader.gif';

const Backdrop = props => {
    if (props.show) {
        return (
            <Fragment>
                <div className="Backdrop" onClick={props.clicked}/>
                <img className="preloader" src={Preloader} alt="preloader"/>
            </Fragment>
        )
    } else {
        return null;
    }
};

export default Backdrop;