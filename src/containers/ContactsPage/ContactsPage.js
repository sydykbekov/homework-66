import React, {Component} from 'react';
import Content from '../../components/Content/Content';
import axios from "axios/index";
import withLoader from "../../hoc/withLoader";

class ContactsPage extends Component {
    state = {
        info: {},
        loading: true
    };

    componentDidMount() {
        axios.get('/contacts.json').then(response => {
            this.setState({info: response.data, loading: false});
        });
    }

    render() {
        return (
            <Content title={this.state.info.title} content={this.state.info.content}/>
        )
    }
}

export default withLoader(ContactsPage, axios);