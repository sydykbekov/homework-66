import React, {Component} from 'react';
import Editor from '../../components/Editor/Editor';
import axios from 'axios';
import withLoader from "../../hoc/withLoader";

class AdminPage extends Component {
    state = {
        page: '',
        title: '',
        content: ''
    };

    pageName = (event) => {
        const page = event.target.value;
        axios.get(`/${event.target.value.toLowerCase()}.json`).then(response => {
            this.setState({
                page: page,
                title: response.data.title,
                content: response.data.content
            });
        });
    };

    pageTitle = (event) => {
        this.setState({title: event.target.value});
    };

    pageContent = (event) => {
        this.setState({content: event.target.value});
    };

    sendContent = () => {
        const object = {
            title: this.state.title,
            content: this.state.content
        };
        axios.put(`/${this.state.page.toLowerCase()}.json`, object).then(() => {
            if (this.state.page === 'Home') {
                this.props.history.push({
                    pathname: '/'
                });
            } else {
                this.props.history.push({
                    pathname: `/${this.state.page.toLowerCase()}`
                });
            }
        });
    };

    render() {
        return (
            <Editor page={this.pageName} title={this.pageTitle}
                    titleValue={this.state.title}
                    content={this.pageContent} contentValue={this.state.content}
                    send={this.sendContent}/>
        )
    }
}

export default withLoader(AdminPage, axios);