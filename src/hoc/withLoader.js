import React, {Component, Fragment} from 'react';
import axios from 'axios';
import Backdrop from "../components/UI/Backdrop/Backdrop";

const withLoader = (WrappedComponent) => {
    return class extends Component {
        constructor(props) {
            super(props);

            this.state = {
                loading: false
            };

            this.state.requestInterceptorID = axios.interceptors.request.use((req) => {
                this.setState({loading: true});
                return req;
            });

            this.state.responseInterceptorID = axios.interceptors.response.use(res => {
                this.setState({loading: false});
                return res;
            });
        }

        componentWillUnmount() {
            axios.interceptors.request.eject(this.state.requestInterceptorID);
            axios.interceptors.response.eject(this.state.responseInterceptorID);
        }

        closeBackdrop = () => {
            this.setState({loading: false});
        };

        render() {
            return (
                <Fragment>
                    <Backdrop show={this.state.loading} clicked={this.closeBackdrop}/>
                    <WrappedComponent {...this.props} />
                </Fragment>
            );
        }
    }
};

export default withLoader;
